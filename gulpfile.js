var gulp = require('gulp'),
    less = require('gulp-less'),
    concatCss = require('gulp-concat-css'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat');

gulp.task('css', function() {
  return gulp.src('assets/css/*.css')
    .pipe(autoprefixer({
			browsers: ['last 32 versions'],
			cascade: false
		}))
    .pipe(concatCss("all.css"))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('app/style/'));
});

gulp.task('less', function () {
  return gulp.src('assets/less/*.less')
    .pipe(less())
    .pipe(gulp.dest('assets/css/'));
});

// js
gulp.task('js', function() {
  gulp.src([
    'assets/js/jquery.js',
    'assets/js/owlSlider.js',
    'assets/js/app.js', 
    ])
    .pipe(uglify())
    .pipe(gulp.dest('assets/js/concat/'))
});

gulp.task('concat', function(){
    gulp.src([
      'assets/js/concat/jquery.js',
      'assets/js/concat/owlSlider.js',
      'assets/js/concat/app.js'
    ])
    .pipe(concat('all.js'))
    .pipe(gulp.dest('app/js/'))
});

// watch OPEN //
gulp.task('default', function() {
  // js
  gulp.watch([
    'assets/js/jquery.js',
    'assets/js/owlSlider.js',
    'assets/js/app.js',
    ], ['js']);
  // styles
  gulp.watch('assets/less/*.less', ['less']);
  gulp.watch('assets/js/concat/*.js', ['concat']);
  gulp.watch('assets/css/*.css', ['css']);
});


















