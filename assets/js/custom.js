$(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //$('.date-time').datetimepicker({
    //    format: 'DD.MM.YYYY HH:mm'
    //});

    $('.map-single').colorbox({
        iframe: true,
        width: '80%',
        height: '80%',
        onComplete: function () {
            new GMaps({
                div: '#cboxLoadedContent',
                lat: -12.043333,
                lng: -77.028333
            });
        }
    });
    if ($('#map-search').length > 0)
        new GMaps({
            div: '#map-search',
            lat: -12.043333,
            lng: -77.028333
        });
    $('.edit-pencil').colorbox({
        width: "70%",
        height: "50%",
        overlayClose: false,
        onClosed: function () {
            location.reload();
        }
    });

    $('.ajax_form').on('submit', function (e) {
        e.preventDefault();
        var self = $(this);
        var button = self.find('*[type=submit]');
        var loading = self.find('.loading');
        loading.show();
        button.hide();
        $.post(self.attr('action'), self.serialize(), function (data) {
            loading.hide();
            button.show();
            self.trigger('reset');
            if (data.success = 1)
                alert(self.find('.sent_msg').text());
        }).error(function (data) {
            var errors = $.parseJSON(data.responseText);
            self.find('*[name=' + Object.keys(errors)[0] + ']').focus();
            alert(errors[Object.keys(errors)[0]]);
            loading.hide();
            button.show();
        });
    });

    var body = $('body');

    var services_container = $('#services-container');
    $('.add-service').on('click', function (e) {
        var template = $('#service-template').html();
        Mustache.parse(template);   // optional, speeds up future uses
        var rendered = Mustache.render(template);

        services_container.append(rendered);
        services_container.find('.service-select').select2({
            templateResult: addUserPic,
            templateSelection: addUserPic,
            placeholder: 'Выберите значение'
        });
        e.preventDefault();
    });

    $('.add-hall').on('click', function (e) {
        var container = $('.halls-container');
        var template = $('#hall-template').html();
        Mustache.parse(template);   // optional, speeds up future uses
        $.get('/ajax/halls/create', {hotel_id: $(this).data('hotel-id')}, function (data) {
            var rendered = Mustache.render(template, {hall: data.hall});
            container.append(rendered);
            $('.x-editable').editable();
            $('.x-editable-st').editable({
                params: function (params) {
                    params.hall_id = $(this).data('hall-id');
                    return params;
                }
            });
            initUpload.init();
        });
        e.preventDefault();
    });

    body.on('click', '.add-benefit', function (e) {
        var self = $(this);
        var container = self.prev();
        var template = $('#hall-benefit-template').html();
        $.get('/ajax/hall/benefit/create', {hall_id: $(this).data('hall-id')}, function (data) {
            var rendered = Mustache.render(template, {benefit: data.benefit});
            container.append(rendered);
            $('.x-editable').editable();
        });
        e.preventDefault();
    });

    services_container.find('.service-select').select2({
        templateResult: addUserPic,
        templateSelection: addUserPic,
        placeholder: 'Выберите значение'
    });


    body.on('click', '.remove-service', function (e) {
        var self = $(this);
        self.closest('.service-item').remove();
    });
    body.on('click', '.remove-hall', function (e) {
        var self = $(this);
        $.get('/ajax/halls/remove', {hotel_id: $(this).data('hotel-id'), hall_id: self.data('hall-id')}, function (data) {
            self.closest('.row').remove();
        });
    });
    body.on('click', '.remove-benefit', function (e) {
        var self = $(this);
        $.get('/ajax/hall/benefit/remove', {benefit_id: self.data('benefit-id')}, function (data) {
            self.closest('i').remove();
        });
    });

    body.on('click', '.save-service', function (e) {
        var self = $(this);
        var form = self.closest('form');
        $.post(form.attr('action'), form.serialize(), function (data) {
            alert(data.msg);
        });
    });

    var chosen = $('#chosen-count');
    $('.basket-add-hall').on('click', function (e) {
        var self = $(this);
        $.get('/ajax/basket/add', {hall_id: self.data('id'), hotel_id: self.data('hotel-id')}, function (data) {
            chosen.text(data.chosen);
            alert(data.msg);
        });
        e.preventDefault();
    });


    var initUpload = {
        init: function () {
            $('.imageUpload').each(function (index, item) {
                var $item = $(item);
                var $group = $item.closest('.form-group');
                var $errors = $item.find('.errors');
                var $noValue = $item.find('.no-value');
                var $hasValue = $item.find('.has-value');
                var $thumbnail = $item.find('img');
                var $input = $item.find('.imageValue');
                var flow = new Flow({
                    target: $item.data('target'),
                    testChunks: false,
                    chunkSize: 1024 * 1024 * 1024,
                    query: {
                        _token: $item.data('token'),
                        entity_id: $item.data('entity-id'),
                        seating_type: $item.data('seating-type-id')
                    }
                });
                flow.assignBrowse($item, false, true);
                flow.on('filesSubmitted', function (file) {
                    flow.upload();
                });
                flow.on('fileSuccess', function (file, message) {
                    flow.removeFile(file);

                    $errors.html('');
                    $group.removeClass('has-error');

                    var result = $.parseJSON(message);
                    $thumbnail.attr('src', result.url);
                    $hasValue.find('span').text(result.value);
                    $input.val(result.value);
                    $noValue.addClass('hidden');
                    $hasValue.removeClass('hidden');
                });
                flow.on('fileError', function (file, message) {
                    flow.removeFile(file);

                    var response = $.parseJSON(message);
                    var errors = '';
                    $.each(response, function (index, error) {
                        errors += '<p class="help-block">' + error + '</p>'
                    });
                    $errors.html(errors);
                    $group.addClass('has-error');
                });
                $item.find('.imageRemove').click(function () {
                    $input.val('');
                    $noValue.removeClass('hidden');
                    $hasValue.addClass('hidden');
                });
            });
        }
    };

    initUpload.init();

    function addUserPic(opt) {
        if (!opt.id) {
            return opt.text;
        }
        var optimage = $(opt.element).data('image');
        if (!optimage) {
            return opt.text;
        } else {
            var $opt = $(
                '<span class="userName"><img src="' + optimage + '" class="userPic" /> ' + $(opt.element).text() + '</span>'
            );
            return $opt;
        }
    }

    $('.remove-popup').on('click', function (e) {
        $(this).closest('.holder').remove();
    });

    $(".people_count").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    /*
     *
     * Checkbox on placement view
     *
     * */
    $('#placement-req').change(function () {
        var entry = $('.placement-required');
        if ($(this).is(":checked")) {
            entry.show();
            return;
        }
        entry.hide();
    });

    //$('.search_form').on('submit', function(e){
    //    var self = $(this);
    //    var people = self.find('.people_count').val();
    //    if(people > 1500)
    //        return false;
    //    return true;
    //});
});