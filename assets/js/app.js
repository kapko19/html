$(function () {
    //------------ VARS ------------//
    var thumb = $('.tumb .item'),    
        item = $('.large .item'),
        active = 'active',
        top = $('.tumb .top'),
        bottom = $('.tumb .bottom'),
        parent = $('.item-parent'),
        parentLength = $(thumb).length,
        i = 0,
        heightTop = $(thumb).height() + 15,
        large = $('.large-slide'),
        widWindows = $(window).width(),
        // search //
        search = $('header .search'),
        searchForm = $('header .search-form'),
        inputSearch = $('header .search-form input'),
        // navigation //
        nav = $('.navigate'),
        mp = $('.mp');


    //------------ METHODS ------------//
    function slider(){
        var index = $(this).index();
            activeSlide = $(item)[index];

        $(item).removeClass(active);
        $(thumb).removeClass(active);
        $(this).addClass(active);
        $(activeSlide).addClass(active);
    }

    function SlideTumb(){
        var thisClass = $(this).attr('class');
        if(thisClass === 'bottom'){
            i+=(i>parentLength - 5)?0:1;
        }else{
            i-=(i<=0)?0:1;
        }
        var mar = heightTop * i;
        $(parent).css('margin-top', - mar + 'px');
    }
    function searMethod(){
        $(searchForm).show();
        $(inputSearch).focus();
    }
    function hideSearch(){
        $(searchForm).hide();
    }
    function navigationFnt(){
        $(nav).fadeIn(400);
        $('html').css('overflow-y', 'hidden');
    }
    // console.log(increment);
    //------------ INTIT ------------//
    $(thumb).click(slider);
    $(search).click(searMethod);
    $(inputSearch).focusout(hideSearch);
    // tumbNails //
    if(parentLength>4){
        $(top).click(SlideTumb);
        $(bottom).click(SlideTumb);
    }
    // navigate //
    $(mp).click(navigationFnt);
    $(document).on('click', '.navigate', function (e) {
        if (!$(e.target).closest(".nav-in").length) {
            $(nav).fadeOut(400);
            $('html').css('overflow-y', 'auto');
        }
        e.stopPropagation();
    });

   
    // // RESPONSE max-width > 768 //
    // if(widWindows < 770){
    //     $(navigationLi).click(navLiSubmenu);
    // }
    // // RESPONSE max-width < 992px //
    if(widWindows < 770){
        $(large).owlCarousel({
            margin: 0,
            nav: false,
            pagination: false,
            autoplay:true,
            navSpeed: true,
            autoplayTimeout: 2000,
            responsive: {
                0: {
                    items: 1
                }
            }
        });
        $('.tumb').remove();
    }
    // menu Closed //
    // $(document).on('click', 'section', function (e) {
    //     if (!$(e.target).closest("nav").length) {
    //         $(navigation).addClass('active_nav'); 
    //     }
    //     e.stopPropagation();
    // });

    // PLUGINS //
    $('.slide-popular').owlCarousel({
        margin: 1,
        nav: true,
        pagination: false,
        autoplay:true,
        navSpeed: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            800: {
                items: 3
            },
            1000: {
                items: 4
            },
            1200: {
                items: 4
            }
        }
    });
    $('.banner-slider').owlCarousel({
        margin: 0,
        nav: true,
        pagination: true,
        autoplay:true,
        navSpeed: true,
        responsive: {
             0: {
                items: 1
            },
            600: {
                items: 1
            },
            800: {
                items: 1
            },
            1000: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    });

});
